#!/bin/bash
php bin/console cache:clear --env=prod
php bin/console cache:clear --env=dev
php bin/console assets:install --symlink --relative
chmod -R 777 var/cache