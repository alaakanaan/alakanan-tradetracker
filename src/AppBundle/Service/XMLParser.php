<?php

namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\Container;


class XMLParser
{

    private $encoding;
    private $parser;
    private $result;
    private $currentTag;
    private $currentAttr;
    private $feed_labels;
    private $extracted_fields;
    private $root_field;
    private $parent_field;
    private $read_length;

    public function __construct()
    {
        $this->encoding = 'UTF-8';
        $this->read_length = 5000;// reading limit in bytes # 5kB
        $this->parser = xml_parser_create($this->encoding);
        $this->result['products'] = [];
        $this->currentAttr = '';
        $this->currentTag = '';
        $this->feed_labels = ['ProductID', 'Name', 'Price & Currency', 'ProductURL', 'ImageURL', 'Description', 'Category (all)'];
        $this->root_field = 'PRODUCTS';
        $this->parent_field = 'PRODUCT';
        $this->extracted_fields = ['PRODUCTID', 'NAME', 'DESCRIPTION', 'PRICE', 'CATEGORY', 'PRODUCTURL', 'IMAGEURL'];

        xml_set_object($this->parser, $this);
        xml_set_element_handler($this->parser, 'startElement', 'endElement');
        xml_set_character_data_handler($this->parser, 'character_data');
    }


    public function parse($url)
    {
        if (!$url) {
            return false;
        }

        //@todo add offset to the url for paging and handling the large data view in the user browser
        //binds a named resource, specified by url, to a stream.
        $fp = fopen($url, 'r')
        or die ("<p>Please insert a valid url in the form and click on fetch feed.</p>");

        while ($data = fread($fp, $this->read_length)) {
            xml_parse($this->parser, $data) or
            die(sprintf('XML ERROR: %s at line %d',
                xml_error_string(xml_get_error_code($this->parser)),
                xml_get_current_line_number($this->parser)));
        }


        xml_parser_free($this->parser);
    }

    public function startElement($parser, $element_name, $attr)
    {
        $this->currentTag = $element_name;

        $this->currentAttr = $attr;

        if (in_array($element_name, $this->extracted_fields)) {
            if ($element_name == "IMAGEURL") {
                echo '<td>';
            } else {
                echo '<td class="feed-data more">';
            }
        } elseif ($element_name == $this->parent_field) {
            echo '<tr>';
        }
    }

    private function endElement($parser, $element_name)
    {

        if (in_array($element_name, $this->extracted_fields)) {
            echo '</td>';
        } elseif ($element_name == $this->parent_field) {
            echo '</tr>';
        }
        $this->currentTag = "";
        $this->currentAttr = "";
    }


    private function character_data($parser, $data)
    {
        switch ($this->currentTag) {
            case 'PRODUCTID':
            case 'NAME':
            case 'CATEGORY':
            case 'DESCRIPTION':
            case 'PRODUCTURL':
                echo $data;
                break;
            case 'PRICE':
                echo $data . ' ' . $this->currentAttr['CURRENCY'];
                break;
            case 'IMAGEURL':
                echo "<img width='70%' src=" . $data . '>';
                break;
        }

    }

    /**
     * get feed fields label
     * @return array
     */
    public function getLabels()
    {
        return $this->feed_labels;
    }

}