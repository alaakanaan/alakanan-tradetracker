<?php
/**
 * Created by PhpStorm.
 * User: alaa
 * Date: 4/16/17
 * Time: 4:03 AM
 */
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;

class FeedType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('feed_url', UrlType::class, ['attr' => ['placeholder' => 'Please insert a url']]);
        $builder->add('fetch_feed', SubmitType::class, ['attr' => ['class' => 'btn btn-primary btn-tradetracker']]);
    }

}