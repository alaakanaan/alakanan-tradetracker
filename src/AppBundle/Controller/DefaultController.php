<?php

namespace AppBundle\Controller;

use AppBundle\Form\FeedType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    /**
     * application home page
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        $url = false;
        $form = $this->createForm(FeedType::class, null, array(
            'action' => $this->generateUrl('homepage'),
            'method' => 'GET',
        ));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $url = $form->get('feed_url')->getData();
        }
        return $this->render('AppBundle:homepage:index.html.twig', [
            'form' => $form->createView(),
            'url' => $url
        ]);
    }
}
