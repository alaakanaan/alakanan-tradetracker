alakanan-tradetracker
======================

TradeTracker XML Assessment Parser Build Using Symfony3.


Installation
------------

After download the source file, please run the "composer update" and you should be good to go.

Note
------------

In the appkernel (/app/AppKernel.php) you will find the php init properties:

- error_reporting(E_STRICT)
- ini_set('memory_limit', '32M')